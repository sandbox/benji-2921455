<?php

namespace Drupal\field_clone;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Field\FieldTypePluginManager;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class FieldClone.
 */
class FieldClone implements FieldCloneInterface {

  /**
   * The name of the entity type.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new FieldClone object.
   */
  public function __construct(EntityManager $entity_manager, FieldTypePluginManager $field_type_plugin_manager, ConfigFactory $config_factory) {
    $this->entityManager = $entity_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityType($entity_type_id) {
    $this->entityTypeId = $entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setBundle($bundle) {
    $this->bundle = $bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function addField($field_name, $label, $field_storage_type, $translatable) {
    // Check if we're dealing with a preconfigured field.
    if (strpos($field_storage_type, 'field_ui:') === 0) {
      list(, $field_type, $preset_key) = explode(':', $field_storage_type, 3);
      $default_options = $this->getNewFieldDefaults($field_type, $preset_key);
    }
    else {
      $field_type = $field_storage_type;
      $default_options = [
        'entity_form_display' => ['default' => []],
        'entity_view_display' => ['default' => []],
      ];
    }

    // Set values for the field storage to be created.
    $field_storage_values = [
      'field_name' => $field_name,
      'type' => $field_type,
      'entity_type' => $this->entityTypeId,
      'translatable' => $translatable,
    ];
    if (isset($default_options['field_storage_config'])) {
      $field_storage_values += array_intersect_key(
        $default_options['field_storage_config'],
        ['cardinality' => 0, 'settings' => 1]
      );
    }

    // Set values for the field to be created.
    $field_values = [
      'entity_type' => $this->entityTypeId,
      'bundle' => $this->bundle,
      'field_name' => $field_name,
      'label' => $label,
      // Field translatability should be explicitly enabled by the users.
      'translatable' => FALSE,
    ];
    if (isset($default_options['field_config'])) {
      $field_values += array_intersect_key(
        $default_options['field_config'], [
          'description' => 0,
          'settings' => 0,
          'required' => 0,
          'default_value' => 0,
          'default_value_callback' => 0,
        ]
      );
    }

    // Create the field storage.
    $this->entityManager->getStorage('field_storage_config')
      ->create($field_storage_values)->save();

    // Create the field.
    $field = $this->entityManager->getStorage('field_config')
      ->create($field_values);
    $field->save();

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneField($field_name, $label, $source_bundle = NULL, bool $copy_form_modes = TRUE, bool $copy_view_modes = TRUE) {
    $default_options = $this->getExistingFieldDefaults($field_name);

    $field_values = [
      'entity_type' => $this->entityTypeId,
      'bundle' => $this->bundle,
      'field_name' => $field_name,
      'label' => $label,
    ];
    if (isset($default_options['field_config'])) {
      $field_values += array_intersect_key(
        $default_options['field_config'], [
          'description' => 0,
          'settings' => 0,
          'required' => 0,
          'default_value' => 0,
          'default_value_callback' => 0,
        ]
      );
    }

    // Create the field.
    $field = $this->entityManager->getStorage('field_config')
      ->create($field_values);
    $field->save();

    // Configure the display modes.
    if ($copy_form_modes && !empty($default_options['entity_form_display'])) {
      $this->configureEntityFormDisplay($field_name, $default_options['entity_form_display']);
    }
    if ($copy_view_modes && !empty($default_options['entity_view_display'])) {
      $this->configureEntityViewDisplay($field_name, $default_options['entity_view_display']);
    }

    return $field;
  }

  /**
   * Get default options from preconfigured options for a new field.
   *
   * Get the preconfigured options for the corresponding key but nest the
   * options for 'entity_form_display' and 'entity_view_display' one level
   * deeper, with key 'default' (for the display mode).
   *
   * @param string $field_name
   *   The machine name of the field. The field class must implement
   *   Drupal\Core\Field\PreconfiguredFieldUiOptionsInterface.
   * @param string $preset_key
   *   A key in the preconfigured options array for the field.
   *
   * @return array
   *   An array of settings with keys 'field_config', 'entity_form_display', and
   *   'entity_view_display'.
   *
   * @see Drupal\Core\Field\PreconfiguredFieldUiOptionsInterface::getPreconfiguredOptions()
   */
  protected function getNewFieldDefaults($field_name, $preset_key) {
    $field_type_class = $this->fieldTypePluginManager
      ->getDefinition($field_name)['class'];
    $default_options = $field_type_class::getPreconfiguredOptions()[$preset_key];
    // Preconfigured options only apply to the default display modes.
    foreach (['entity_form_display', 'entity_view_display'] as $key) {
      if (isset($default_options[$key])) {
        $default_options[$key] = ['default' => $default_options[$key]];
      }
      else {
        $default_options[$key] = ['default' => []];
      }
    }

    return $default_options;
  }

  /**
   * Get default options from an existing field and bundle.
   *
   * @param string $field_name
   *   The machine name of the field.
   * @param string $existing_bundle
   *   (optional) The name of the existing bundle (not $this->bundle).
   *
   * @return array
   *   An array of settings with keys 'field_config', 'entity_form_display', and
   *   'entity_view_display' if these are defined for the existing field. If the
   *   field is not defined for the specified bundle (or for any bundle if
   *   $existing_bundle is omitted) then return an empty array.
   */
  protected function getExistingFieldDefaults($field_name, $existing_bundle = '') {
    $default_options = [];
    $field_map = $this->entityManager->getFieldMap();
    if ($existing_bundle) {
      // Validate that $existing_bundle is different from $this->bundle and that
      // the field is defined for the supplied bundle.
      if ($existing_bundle == $this->bundle) {
        return [];
      }
      if (empty($field_map[$this->entityTypeId][$field_name]['bundles'][$existing_bundle])) {
        return [];
      }
    }
    else {
      // If $existing_bundle is not supplied, then find a suitable candidate.
      $existing_bundle = $this->getExistingBundle($field_name);
    }

    // Copy field configuration.
    $existing_field = $this->entityManager
      ->getFieldDefinitions($this->entityTypeId, $existing_bundle)[$field_name];
    $default_options['field_config'] = [
      'description' => $existing_field->get('description'),
      'settings' => $existing_field->get('settings'),
      'required' => $existing_field->get('required'),
      'default_value' => $existing_field->get('default_value'),
      'default_value_callback' => $existing_field->get('default_value_callback'),
    ];

    // Copy form and view mode configuration.
    $properties = [
      'targetEntityType' => $this->entityTypeId,
      'bundle' => $existing_bundle,
    ];
    $existing_forms = $this->entityManager
      ->getStorage('entity_form_display')
      ->loadByProperties($properties);
    foreach ($existing_forms as $type => $form) {
      if ($settings = $form->getComponent($field_name)) {
        $default_options['entity_form_display'][$form->getMode()] = $settings;
      }
    }
    $existing_views = $this->entityManager
      ->getStorage('entity_view_display')
      ->loadByProperties($properties);
    foreach ($existing_views as $type => $view) {
      if ($settings = $view->getComponent($field_name)) {
        $default_options['entity_view_display'][$view->getMode()] = $settings;
      }
    }

    return $default_options;
  }

  /**
   * Find a bundle to which the given field is already attached.
   *
   * @param string $field_name
   *   The name of the field.
   */
  protected function getExistingBundle($field_name) {
    // @todo Consistently choose the bundle. Oldest? Newest? Alphabetical?
    $field_map = $this->entityManager->getFieldMap();
    if (empty($field_map[$this->entityTypeId][$field_name]['bundles'])) {
      return '';
    }
    $bundles = $field_map[$this->entityTypeId][$field_name]['bundles'];
    return reset($bundles);
  }

  /**
   * Configures the field for the default form mode.
   *
   * @param string $field_name
   *   The field name.
   * @param array[] $settings
   *   (optional) an array of settings, keyed by form mode. Defaults to [].
   */
  protected function configureEntityFormDisplay($field_name, array $settings = []) {
    // For a new field, only $mode = 'default' should be set. Use the
    // preconfigured or default widget and settings. The field will not appear
    // in other form modes  until it is explicitly configured. When an existing
    // field is reused, copy the behavior for all form modes.
    $enabled_modes = [];
    $form_modes = $this->entityManager->getFormModes('node');
    foreach ($settings as $mode => $options) {
      $form = entity_get_form_display($this->entityTypeId, $this->bundle, $mode);
      if (!$form->status()) {
        $form->set('status', TRUE);
        $enabled_modes[] = $form_modes[$mode]['label'];
      }
      $form->setComponent($field_name, $options)->save();
    }

    if ($enabled_modes) {
      drupal_set_message($this->t('The following form mode(s) were enabled: %enabled_modes', [
        '%enabled_modes' => implode(', ', $enabled_modes),
      ]));
    }
  }

  /**
   * Configures the field for the default view mode.
   *
   * @param string $field_name
   *   The field name.
   * @param string[][] $settings
   *   (optional) an array of settings, keyed by view mode. Only the 'type' key
   *   of the inner array is used, and the value should be the plugin ID of a
   *   formatter. Defaults to [].
   */
  protected function configureEntityViewDisplay($field_name, array $settings = []) {
    // For a new field, only $mode = 'default' should be set. Use the
    // preconfigured or default formatter and settings. The field stays hidden
    // for other view modes until it is explicitly configured. When an existing
    // field is reused, copy the behavior for all view modes.
    $enabled_modes = [];
    $view_modes = $this->entityManager->getViewModes('node');
    foreach ($settings as $mode => $options) {
      $view = entity_get_display($this->entityTypeId, $this->bundle, $mode);
      if (!$view->status()) {
        $view->set('status', TRUE);
        $enabled_modes[] = $view_modes[$mode]['label'];
      }
      $view->setComponent($field_name, $options)->save();
    }

    if ($enabled_modes) {
      drupal_set_message($this->t('The following view mode(s) were enabled: %enabled_modes', [
        '%enabled_modes' => implode(', ', $enabled_modes),
      ]));
    }
  }

}
