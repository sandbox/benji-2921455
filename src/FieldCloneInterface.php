<?php

namespace Drupal\field_clone;

/**
 * Interface FieldCloneInterface.
 */
interface FieldCloneInterface {

  /**
   * Set the entity type.
   *
   * @param string $entity_type_id
   *   The name of the entity type.
   */
  public function setEntityType($entity_type_id);

  /**
   * Set the entity bundle.
   *
   * @param string $bundle
   *   The name of the entity bundle.
   */
  public function setBundle($bundle);

  /**
   * Create new field storage and field.
   *
   * @param string $field_name
   *   The name of the field to clone.
   * @param string $label
   *   The label to use for the new field.
   * @param string $field_storage_type
   *   The type of field to create.
   * @param bool $translatable
   *   Whether the field storage should be translatable. The field itself is
   *   initially not translatable in all cases.
   *
   * @return Field
   *   A Field entity, or an exception if the field or field storage could not
   *   be created.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   */
  public function addField($field_name, $label, $field_storage_type, $translatable);

  /**
   * Clone a field from a bundle to which it is already attached.
   *
   * @param string $field_name
   *   The name of the field to clone.
   * @param string $label
   *   The label to use for the new field.
   * @param string $source_bundle
   *   (optional) The name of the bundle from which to copy configuration. If
   *   not set, then use the global defaults for the field type.
   * @param bool $copy_form_modes
   *   (optional) If TRUE, then copy form modes from the field on the source
   *   bundle.
   * @param bool $copy_view_modes
   *   (optional) If TRUE, then copy view modes from the field on the source
   *   bundle.
   *
   * @return Field|Exception
   *   A Field entity, or an exception if the field could not be created.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   */
  public function cloneField($field_name, $label, $source_bundle = NULL, bool $copy_form_modes = FALSE, bool $copy_view_modes = FALSE);

}
