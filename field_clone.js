(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.fieldCloneFieldAddForm = {
    attach: function attach(context) {
      var $form = $(context).find('[data-drupal-selector="field-clone-field-add-form"]').once('field_clone_add');
      if ($form.length) {
        $form.find('.js-form-item-label label,' + '.js-form-item-field-name label,' + '.js-form-item-existing-storage-label label').addClass('js-form-required form-required');

        var $newFieldType = $form.find('select[name="new_storage_type"]');
        var $existingStorageName = $form.find('select[name="existing_storage_name"]');
        var $existingStorageLabel = $form.find('input[name="existing_storage_label"]');

        $newFieldType.on('change', function () {
          if ($(this).val() !== '') {
            $existingStorageName.val('').trigger('change');
          }
        });

        $existingStorageName.on('change', function () {
          var value = $(this).val();
          if (value !== '') {
            $newFieldType.val('').trigger('change');

            if (typeof drupalSettings.existingFieldLabels[value] !== 'undefined') {
              $existingStorageLabel.val(drupalSettings.existingFieldLabels[value]);
            }
          }
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
